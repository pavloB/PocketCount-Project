package com.turtledev.pockercounter.netWork;


public interface NetworkCallback {
    void onSuccess(Object response);
    void onFailure(String error);
}
