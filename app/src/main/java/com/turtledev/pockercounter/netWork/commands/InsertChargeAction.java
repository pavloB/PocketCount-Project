package com.turtledev.pockercounter.netWork.commands;

import android.util.Log;

import com.turtledev.pockercounter.models.InsertCharge;
import com.turtledev.pockercounter.netWork.NetworkApi;
import com.turtledev.pockercounter.netWork.NetworkCallback;

/**
 * Created by 1 on 06.06.2016.
 */
public class InsertChargeAction {
    private static final String TAG = InsertChargeAction.class.getSimpleName();

    private final NetworkApi mNetworkApi;
    InsertCharge insertCharge;
    NetworkCallback mNetworkCallback;
    int mRetries;
    private String userToken;


    public InsertChargeAction(String userToken,InsertCharge insertCharge, NetworkCallback mNetworkCallback, int mRetries ) {
        this.mNetworkApi = NetworkApi.getInstance();
        this.userToken=userToken;
        this.mRetries = mRetries;
        this.mNetworkCallback = mNetworkCallback;
        this.insertCharge = insertCharge;
    }

    public void run(){

        mNetworkApi.postCharges(insertCharge, new NetworkCallback() {


            @Override
            public void onSuccess(Object response) {
                mNetworkCallback.onSuccess(response);
            }

            @Override
            public void onFailure(String error) {
                if (mRetries > 0){
                    Log.v(TAG, "charges retry " + mRetries);
                    mRetries--;
                    run();
                } else {
                    mNetworkCallback.onFailure(error);
                }
            }
        });
    }
}
