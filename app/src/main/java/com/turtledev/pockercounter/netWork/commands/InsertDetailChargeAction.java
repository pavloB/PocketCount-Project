package com.turtledev.pockercounter.netWork.commands;

import android.util.Log;

import com.turtledev.pockercounter.models.DetailCharge;
import com.turtledev.pockercounter.netWork.NetworkApi;
import com.turtledev.pockercounter.netWork.NetworkCallback;

/**
 * Created by 1 on 09.07.2016.
 */
public class InsertDetailChargeAction {
    private static final String TAG = InsertChargeAction.class.getSimpleName();

    private final NetworkApi mNetworkApi;
    DetailCharge detailCharge;
    NetworkCallback mNetworkCallback;
    int mRetries;
    private String userToken;


    public InsertDetailChargeAction(String userToken, DetailCharge detailCharge, NetworkCallback mNetworkCallback, int mRetries ) {
        this.mNetworkApi = NetworkApi.getInstance();
        this.userToken=userToken;
        this.mRetries = mRetries;
        this.mNetworkCallback = mNetworkCallback;
        this.detailCharge = detailCharge;
    }

    public void run(){

        mNetworkApi.postDetailCharges(detailCharge, new NetworkCallback() {


            @Override
            public void onSuccess(Object response) {
                mNetworkCallback.onSuccess(response);
            }

            @Override
            public void onFailure(String error) {
                if (mRetries > 0){
                    Log.v(TAG, "charges retry " + mRetries);
                    mRetries--;
                    run();
                } else {
                    mNetworkCallback.onFailure(error);
                }
            }
        });
    }
}
