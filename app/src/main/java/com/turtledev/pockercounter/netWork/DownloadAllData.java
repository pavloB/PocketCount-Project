package com.turtledev.pockercounter.netWork;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.turtledev.pockercounter.MainActivity;
import com.turtledev.pockercounter.mDataBase.DBAdapter;
import com.turtledev.pockercounter.models.ChargesDetailResponse;
import com.turtledev.pockercounter.models.ChargesResponse;
import com.turtledev.pockercounter.netWork.commands.ChargesAction;
import com.turtledev.pockercounter.netWork.commands.DetailChargesAction;

/**
 * Created by 1 on 14.07.2016.
 */
public class DownloadAllData extends AsyncTask<String, Void, Void> {
    private static final int MAX_RETRIES = 2;

    Context context;
    DBAdapter dbAdapter;
    ProgressDialog pd;

    public DownloadAllData(Context context, DBAdapter dbAdapter) {
        this.context = context;
        this.dbAdapter = dbAdapter;

    }


    @Override
    protected Void doInBackground(String... token) {
        String userToken = token[0];
        getCharges(userToken);
        return null;
    }

    protected void onPreExecute() {
        super.onPreExecute();

        pd = new ProgressDialog(context);
        pd.setTitle("Download");
        pd.setMessage("Downloading... please wait");
        pd.show();

    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        pd.dismiss();
    }


    private void getCharges(final String userToken) {

        ChargesAction action = new ChargesAction(userToken, new NetworkCallback() {
            @Override
            public void onSuccess(Object response) {
                ChargesResponse chargesDict = (ChargesResponse) response;
                dbAdapter.addServerDataChargesToDB(chargesDict.getData());
                getDetailCharges(userToken);

            }

            @Override
            public void onFailure(String error) {

            }
        }, MAX_RETRIES);
        action.run();


    }

    private void getDetailCharges(String userToken) {

        DetailChargesAction action = new DetailChargesAction(userToken, new NetworkCallback() {
            @Override
            public void onSuccess(Object response) {
                Log.d("1111", " in  getCharges onSuccess");
                ChargesDetailResponse detailsCharges = (ChargesDetailResponse) response;
                dbAdapter.addServerDataDetailsToDB(detailsCharges.getData());

                Intent intent = new Intent(context, MainActivity.class);
                context.startActivity(intent);


            }

            @Override
            public void onFailure(String error) {


            }
        }, MAX_RETRIES);

        action.run();
    }
}
