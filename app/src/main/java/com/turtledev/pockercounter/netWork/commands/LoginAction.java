package com.turtledev.pockercounter.netWork.commands;

import android.util.Log;

import com.turtledev.pockercounter.models.UserLogin;
import com.turtledev.pockercounter.netWork.NetworkApi;
import com.turtledev.pockercounter.netWork.NetworkCallback;


public class LoginAction {
    private static final String TAG = LoginAction.class.getSimpleName();
    private final NetworkApi mNetworkApi;
    UserLogin mUserLogin;
    NetworkCallback mNetworkCallback;
    int mRetries;

    public LoginAction(UserLogin userLogin, NetworkCallback networkCallback, int retries) {
        mUserLogin = userLogin;
        mRetries = retries;
        mNetworkCallback = networkCallback;
        mNetworkApi = NetworkApi.getInstance();
    }

    public void run(){
        mNetworkApi.login(mUserLogin, new NetworkCallback() {
            @Override
            public void onSuccess(Object response) {
                mNetworkCallback.onSuccess(response);
            }

            @Override
            public void onFailure(String error) {
                if (mRetries > 0){
                    Log.v(TAG, "Login retry " + mRetries);
                    mRetries--;
                    run();
                } else {
                    mNetworkCallback.onFailure(error);
                }
            }
        });
    }
}
