package com.turtledev.pockercounter.netWork;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.turtledev.pockercounter.models.ChargesDetailResponse;
import com.turtledev.pockercounter.models.ChargesResponse;
import com.turtledev.pockercounter.models.DetailCharge;
import com.turtledev.pockercounter.models.InsertCharge;
import com.turtledev.pockercounter.models.LoginResponse;
import com.turtledev.pockercounter.models.UserLogin;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class NetworkApi {


    private static NetworkApi instance;

    private final RestClient mRestClient;

    public synchronized static NetworkApi getInstance(){
        if (instance == null){
            instance = new NetworkApi();
        }
        return instance;
    }

    private NetworkApi() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.backendless.com/v1/")
                .addConverterFactory(GsonConverterFactory.create(gson))

                .build();

        mRestClient = retrofit.create(RestClient.class);
}

    public void login(UserLogin userLogin, final NetworkCallback callback){
        Call<LoginResponse> call = mRestClient.login(userLogin);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    try {
                        callback.onFailure(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                callback.onFailure(t.getMessage());
            }
        });
    }

    public void getChargesDict(String userToken, final NetworkCallback callback){
        Call<ChargesResponse> call = mRestClient.getCharges(userToken);
        call.enqueue(new Callback<ChargesResponse>() {
            @Override
            public void onResponse(Call<ChargesResponse> call, Response<ChargesResponse> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    try {
                        callback.onFailure(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ChargesResponse> call, Throwable t) {
                callback.onFailure(t.getMessage());
            }
        });
    }

    public void postCharges(InsertCharge insertCharge, final NetworkCallback callback){
        Call<InsertCharge> call = mRestClient.insertCharge(insertCharge);
        call.enqueue(new Callback<InsertCharge>() {
            @Override
            public void onResponse(Call<InsertCharge> call, Response<InsertCharge> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    try {
                        callback.onFailure(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<InsertCharge> call, Throwable t) {
                callback.onFailure(t.getMessage());
            }
        });
    }


    public void postDetailCharges(DetailCharge detailCharge, final NetworkCallback callback){
        Call<DetailCharge> call = mRestClient.insertDetailCharge(detailCharge);
        call.enqueue(new Callback<DetailCharge>() {
            @Override
            public void onResponse(Call<DetailCharge> call, Response<DetailCharge> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    try {
                        callback.onFailure(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<DetailCharge> call, Throwable t) {
                callback.onFailure(t.getMessage());
            }
        });
    }


    public void getDetailCharges(String userToken, final NetworkCallback callback){
        Call<ChargesDetailResponse> call = mRestClient.getDetailCharges(userToken);
        call.enqueue(new Callback<ChargesDetailResponse>() {
            @Override
            public void onResponse(Call<ChargesDetailResponse> call, Response<ChargesDetailResponse> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    try {
                        callback.onFailure(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ChargesDetailResponse> call, Throwable t) {
                callback.onFailure(t.getMessage());
            }
        });
    }


}
