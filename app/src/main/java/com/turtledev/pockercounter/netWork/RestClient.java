package com.turtledev.pockercounter.netWork;


import com.turtledev.pockercounter.models.ChargesDetailResponse;
import com.turtledev.pockercounter.models.ChargesResponse;
import com.turtledev.pockercounter.models.DetailCharge;
import com.turtledev.pockercounter.models.InsertCharge;
import com.turtledev.pockercounter.models.LoginResponse;
import com.turtledev.pockercounter.models.UserLogin;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public interface RestClient {

    @Headers({"application-id: 9FB3F995-7D5A-D629-FF78-C5CE1E9DB000",
            "secret-key: 81BC7440-0B2E-F347-FF87-8ADC4FDE7B00",
            "application-type: REST",
            "Content-Type: application/json"})
    @POST("users/login")
    Call<LoginResponse> login(@Body UserLogin userLogin);

    @Headers({"application-id: 9FB3F995-7D5A-D629-FF78-C5CE1E9DB000",
            "secret-key: 81BC7440-0B2E-F347-FF87-8ADC4FDE7B00",
            "application-type: REST",
            "Content-Type: application/json"})
    @GET("data/ChargesDict")
    Call<ChargesResponse> getCharges(@Header("user-token") String userToken);


    @Headers({"application-id: 9FB3F995-7D5A-D629-FF78-C5CE1E9DB000",
            "secret-key: 81BC7440-0B2E-F347-FF87-8ADC4FDE7B00",
            "application-type: REST",
            "Content-Type: application/json"})
    @POST("data/ChargesDict")
    Call<InsertCharge> insertCharge( @Body InsertCharge insertCharge);



    @Headers({"application-id: 9FB3F995-7D5A-D629-FF78-C5CE1E9DB000",
            "secret-key: 81BC7440-0B2E-F347-FF87-8ADC4FDE7B00",
            "application-type: REST",
            "Content-Type: application/json"})
    @GET("data/Charges")
    Call<ChargesDetailResponse> getDetailCharges(@Header("user-token") String userToken);


    @Headers({"application-id: 9FB3F995-7D5A-D629-FF78-C5CE1E9DB000",
            "secret-key: 81BC7440-0B2E-F347-FF87-8ADC4FDE7B00",
            "application-type: REST",
            "Content-Type: application/json"})
    @POST("data/Charges")
    Call<DetailCharge> insertDetailCharge(@Body DetailCharge detailCharge);


}
