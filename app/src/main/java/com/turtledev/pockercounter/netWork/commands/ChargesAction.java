package com.turtledev.pockercounter.netWork.commands;

import android.util.Log;

import com.turtledev.pockercounter.netWork.NetworkApi;
import com.turtledev.pockercounter.netWork.NetworkCallback;


public class ChargesAction {
    private static final String TAG = ChargesAction.class.getSimpleName();
    private final NetworkApi mNetworkApi;

    private String userToken;

    NetworkCallback mNetworkCallback;
    int mRetries;

    public ChargesAction(String userToken, NetworkCallback networkCallback, int retries) {

        this.userToken=userToken;

        mRetries = retries;
        mNetworkCallback = networkCallback;
        mNetworkApi = NetworkApi.getInstance();
    }

    public void run(){

        mNetworkApi.getChargesDict(userToken, new NetworkCallback() {


            @Override
            public void onSuccess(Object response) {
                mNetworkCallback.onSuccess(response);
            }

            @Override
            public void onFailure(String error) {
                if (mRetries > 0){
                    Log.v(TAG, "charges retry " + mRetries);
                    mRetries--;
                    run();
                } else {
                    mNetworkCallback.onFailure(error);
                }
            }
        });
    }
}
