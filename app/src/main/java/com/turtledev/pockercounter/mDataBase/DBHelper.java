package com.turtledev.pockercounter.mDataBase;


import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

/**
 * Created by 1 on 28.05.2016.
 */
public class DBHelper extends SQLiteOpenHelper {


    private Context context;

    //DB
     static final String DATABASE_NAME = "DBCharges";
     private static int DB_VERSION = 2;

    //TableCharges table
     static final String CHARGE_TABLE_NAME = "TableCharges";
     static final String CHARGE_ID ="_id";
     static final String CHARGE_NAME = "Name";


    //TableDetailCharges table
     static final String DETAIL_TABLE_NAME = "TableDetailCharges";
     static final String PARENT_CHARGE_NAME ="parent_charge_name";
     static final String DETAIL_CHARGE_ID ="object_ID";
     static final String DESCRIPTION = "Description";
     static final String TIME = "Time";
     static final String SUM = "Sum";



    //CreateTable
     static final String CREATE_CHARGE_TABLE = "CREATE TABLE "+ CHARGE_TABLE_NAME
            + " ( " + CHARGE_NAME +" STRING PRIMARY KEY ,"
            + CHARGE_ID + " STRING);";

     static final String CREATE_DETAIL_CHARGE_TABLE = "CREATE TABLE "+ DETAIL_TABLE_NAME
             + " ( " + DETAIL_CHARGE_ID + " STRING PRIMARY KEY , "
             + PARENT_CHARGE_NAME + " VARCHAR(255) ,  "
             + DESCRIPTION + " VARCHAR(255) , "
             + TIME + " VARCHAR(255) , "
             + SUM +" REAL);";



    //Drop Table
     static final String DROP_CHARGES_TABLE = "DROP TABLE IF EXISTS " + CHARGE_TABLE_NAME;
     static final String DROP_DETAIL_TABLE = "DROP TABLE IF EXISTS " + DETAIL_TABLE_NAME;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DB_VERSION);
        this.context=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_CHARGE_TABLE);
            db.execSQL(CREATE_DETAIL_CHARGE_TABLE);
            Toast.makeText(context," CREATE DB",Toast.LENGTH_SHORT).show();
        } catch (SQLException e) {
            Toast.makeText(context,"ERROR CREATE DB"+e,Toast.LENGTH_SHORT).show();


        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            Toast.makeText(context," UPDATE DB",Toast.LENGTH_SHORT).show();
            db.execSQL(DROP_CHARGES_TABLE);
            db.execSQL(DROP_DETAIL_TABLE);
            onCreate(db);

        } catch (SQLException e) {
            Toast.makeText(context,"ERROR UPDATE DB"+e,Toast.LENGTH_SHORT).show();
        }

    }
}
