package com.turtledev.pockercounter;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.turtledev.pockercounter.Fragments.AbstractFragment;
import com.turtledev.pockercounter.Fragments.ChargesDictFragment;
import com.turtledev.pockercounter.Fragments.ChargesFragment;
import com.turtledev.pockercounter.Fragments.DetailFragment;
import com.turtledev.pockercounter.RecyclerAdapter.ChargesAdapter;
import com.turtledev.pockercounter.mConstants.myConstants;
import com.turtledev.pockercounter.mDataBase.DBAdapter;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ChargesAdapter.Callbacks {

    ChargesDictFragment chargeDictFragment;
    ChargesFragment chargesFragment;
    AbstractFragment fragment;
    FragmentManager fm;
    FloatingActionButton fab;
    DBAdapter dbAdapter;
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;


    private TabLayout viewPagerTab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dbAdapter = new DBAdapter(this);

        initTabs();





        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getSupportFragmentManager();
                fragment = (AbstractFragment) fm.findFragmentById(fragment.GetContFragment());
                fragment.work();
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        viewPagerTab.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 2:
                        showFragment(chargesFragment);
                        navigationView.getMenu().getItem(0).setChecked(true);
                        break;
                    case 3:
                        showFragment(chargeDictFragment);
                        navigationView.getMenu().getItem(1).setChecked(true);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        chargeDictFragment = new ChargesDictFragment();
        chargesFragment = new ChargesFragment();
        fragment = chargeDictFragment;


        if (savedInstanceState == null) {

            fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(chargeDictFragment.GetContFragment(), chargeDictFragment, chargeDictFragment.getTagFragment());
            ft.commit();

        }
    }

    private void initTabs() {

        viewPagerTab = (TabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.addTab(viewPagerTab.newTab().setText(""));
        viewPagerTab.addTab(viewPagerTab.newTab().setText(""));
        viewPagerTab.addTab(viewPagerTab.newTab().setText("Charges"));
        viewPagerTab.addTab(viewPagerTab.newTab().setText("Charges Dictionary"));

        for (int i = 0; i < viewPagerTab.getTabCount(); i++) {
            if (i > 1) {
                TabLayout.Tab tab = viewPagerTab.getTabAt(i);

                RelativeLayout relativeLayout = (RelativeLayout)
                        LayoutInflater.from(this).inflate(R.layout.tab_layout, viewPagerTab, false);

                TextView tabTextView = (TextView) relativeLayout.findViewById(R.id.tab_title);
                tabTextView.setText(tab.getText());
                tab.setCustomView(relativeLayout);
                tab.select();
            }
        }



        LinearLayout tabStrip = ((LinearLayout) viewPagerTab.getChildAt(0));
        for (int i = 0; i < 2; i++) {


            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
    }

//    private void setupViewPager(ViewPager viewpager) {
//
//        FragmentPagerItemAdapter viewPagerAdapter=new FragmentPagerItemAdapter(getSupportFragmentManager());
//        //viewPagerAdapter.addFragment(chargeDictFragment,"Charges");
//        //viewPagerAdapter.addFragment(chargesFragment,"Charges Dictionary");
//
//        viewpager.setAdapter(viewPagerAdapter);
//        //fragment = chargeDictFragment;
//
//        //TODO write adapter for it
//
//    }


    @Override
    public void onBackPressed() {
        FragmentTransaction ft = fm.beginTransaction();
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        }
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_charges) {
            showFragment(chargesFragment);
            viewPagerTab.getTabAt(2).select();

        } else if (id == R.id.nav_charDictionary) {
            showFragment(chargeDictFragment);
            viewPagerTab.getTabAt(3).select();


        } else if (id == R.id.nav_logout) {
            logOutPress();

        } else if (id == R.id.nav_help) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showFragment(AbstractFragment frag) {

        fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(frag.GetContFragment(), frag, frag.getTagFragment());
        ft.commit();

    }


    public void logOutPress() {
        SharedPreferences sharedPreferences = getSharedPreferences(myConstants.SHARED_PREF, getApplicationContext().MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(myConstants.USER_TOKEN, null);
        editor.commit();
        Intent intent = new Intent(this, LoginActivity.class);

        startActivity(intent);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    public ActionBarDrawerToggle getToggle() {
        return toggle;
    }


    @Override
    public void onChargeSelected(String name) {
        Fragment newDetail = DetailFragment.newInstance(name);
        getSupportFragmentManager().beginTransaction()

                .replace(R.id.container, newDetail)
                .commit();
    }
}
