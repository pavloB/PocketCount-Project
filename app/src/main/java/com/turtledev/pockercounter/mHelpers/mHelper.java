package com.turtledev.pockercounter.mHelpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.turtledev.pockercounter.mConstants.myConstants;

/**
 * Created by 1 on 14.07.2016.
 */
public class mHelper {

    public static void saveUserToken(Context context,String userToken){

        SharedPreferences sharedPreferences = context.getSharedPreferences(myConstants.SHARED_PREF,context.getApplicationContext().MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(myConstants.USER_TOKEN,userToken);
        editor.commit();
    }

    public static String gerUserToken(Context context){

        SharedPreferences sPref = context.getSharedPreferences(myConstants.SHARED_PREF,context.MODE_PRIVATE);
        return sPref.getString(myConstants.USER_TOKEN, null);
 }

}
