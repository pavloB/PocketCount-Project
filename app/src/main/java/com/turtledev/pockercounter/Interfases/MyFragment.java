package com.turtledev.pockercounter.Interfases;


public interface MyFragment {
    int GetContFragment();
    int GetItemFragment();
    String getTagFragment();
    void work();
}
