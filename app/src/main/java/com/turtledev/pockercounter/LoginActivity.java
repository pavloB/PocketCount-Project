package com.turtledev.pockercounter;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.turtledev.pockercounter.mDataBase.DBAdapter;
import com.turtledev.pockercounter.mHelpers.mHelper;
import com.turtledev.pockercounter.models.LoginResponse;
import com.turtledev.pockercounter.models.UserLogin;
import com.turtledev.pockercounter.netWork.DownloadAllData;
import com.turtledev.pockercounter.netWork.NetworkCallback;
import com.turtledev.pockercounter.netWork.commands.LoginAction;




public class LoginActivity extends AppCompatActivity {

    private Button logBtn;
    private EditText etLog;
    private EditText etPas;
    Context context;
    Gson mGson;
    private static final int MAX_RETRIES = 2;
    DBAdapter dbAdapter;






    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        dbAdapter=new DBAdapter(this);
        context=this;
        etLog=(EditText)findViewById(R.id.etLog);
        etPas=(EditText)findViewById(R.id.etPas);
        logBtn=(Button)findViewById(R.id.logBtn);

//        mGson = new GsonBuilder()
//                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
//                .create();

        logBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etLog.length()==0 || etPas.length()==0){
                    Toast.makeText(LoginActivity.this, R.string.LogToast,Toast.LENGTH_SHORT).show();
                }else {
                    new Login(context).execute(etLog.getText().toString(), etPas.getText().toString());

                }
            }
      });
   }






    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }
    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }


    public class Login extends AsyncTask<String,Void,Void>{

        Context context;

        public Login(Context context) {
            this.context = context;
        }

        @Override
        protected Void doInBackground(String... params) {
            String login=params[0];
            String password=params[1];

            retrofitRequest(login,password);
            return null;
        }
        private void retrofitRequest(String login,String password) {

            LoginAction action = new LoginAction(new UserLogin(etLog.getText().toString(), etPas.getText().toString()),
                    new NetworkCallback() {
                        @Override
                        public void onSuccess(Object response) {
                            LoginResponse loginResponse = (LoginResponse) response;
                            String token = loginResponse.getUserToken();
                            mHelper.saveUserToken(context,token);

                            new DownloadAllData(context,dbAdapter).execute(token);



                        }

                        @Override
                        public void onFailure(String error) {

                        }
                    }, MAX_RETRIES);

            action.run();
        }




    }

}
