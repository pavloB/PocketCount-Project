package com.turtledev.pockercounter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.turtledev.pockercounter.mDataBase.DBAdapter;
import com.turtledev.pockercounter.mHelpers.mHelper;
import com.turtledev.pockercounter.netWork.DownloadAllData;

/**
 * Created by 1 on 31.05.2016.
 */
public class SplashScreen extends AppCompatActivity {

    public DBAdapter dbAdapter;


    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.splash);
        dbAdapter=new DBAdapter(this);

        if (mHelper.gerUserToken(this)!=null)
            new DownloadAllData(this,dbAdapter).execute(mHelper.gerUserToken(this));
         else
            startActivity(new Intent(SplashScreen.this,LoginActivity.class));
    }
    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

}