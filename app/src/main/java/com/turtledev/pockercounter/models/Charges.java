package com.turtledev.pockercounter.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class Charges implements Parcelable {
    String name;

    @SerializedName(value = "objectId")
    String objectId;

    public Charges(String newCharge) {
        this.name=newCharge;
    }


    public String getObjectId() {
        return objectId;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.objectId);
    }

    public Charges() {
    }

    public Charges(Parcel in) {
        this.name = in.readString();
        this.objectId=in.readString();
    }

    public static final Creator<Charges> CREATOR = new Creator<Charges>() {
        @Override
        public Charges createFromParcel(Parcel source) {
            return new Charges(source);
        }

        @Override
        public Charges[] newArray(int size) {
            return new Charges[size];
        }
    };
}
