package com.turtledev.pockercounter.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by 1 on 07.07.2016.
 */
public class ChargesDetailResponse implements Parcelable{
    String nextPage;

    List<DetailCharge> data;
    public ChargesDetailResponse(String nextPage, List<DetailCharge> data) {
        this.nextPage = nextPage;
        this.data = data;
    }

    protected ChargesDetailResponse(Parcel in) {
        nextPage = in.readString();
        data = in.createTypedArrayList(DetailCharge.CREATOR);
    }

    public static final Creator<ChargesDetailResponse> CREATOR = new Creator<ChargesDetailResponse>() {
        @Override
        public ChargesDetailResponse createFromParcel(Parcel in) {
            return new ChargesDetailResponse(in);
        }

        @Override
        public ChargesDetailResponse[] newArray(int size) {
            return new ChargesDetailResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nextPage);
        dest.writeTypedList(data);
    }

    public String getNextPage() {
        return nextPage;
    }

    public void setNextPage(String nextPage) {
        this.nextPage = nextPage;
    }

    public List<DetailCharge> getData() {
        return data;
    }

    public void setData(List<DetailCharge> data) {
        this.data = data;
    }

    public static Creator<ChargesDetailResponse> getCREATOR() {
        return CREATOR;
    }
}
