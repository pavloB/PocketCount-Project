package com.turtledev.pockercounter.models;

/**
 * Created by 1 on 06.06.2016.
 */
public class InsertCharge {

    String name;


    public InsertCharge(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }



    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "InsertCharge{" +
                "name='" + name + '\'' +
                '}';
    }
}
