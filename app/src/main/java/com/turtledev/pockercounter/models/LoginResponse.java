package com.turtledev.pockercounter.models;

import com.google.gson.annotations.SerializedName;


public class LoginResponse {
    String name;

    @SerializedName(value = "user-token")
    String userToken;

    String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "name='" + name + '\'' +
                ", userToken='" + userToken + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
