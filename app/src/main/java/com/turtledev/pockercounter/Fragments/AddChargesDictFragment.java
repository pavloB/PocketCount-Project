package com.turtledev.pockercounter.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.turtledev.pockercounter.MainActivity;
import com.turtledev.pockercounter.R;
import com.turtledev.pockercounter.mConstants.myConstants;
import com.turtledev.pockercounter.mDataBase.DBAdapter;
import com.turtledev.pockercounter.models.InsertCharge;
import com.turtledev.pockercounter.netWork.NetworkCallback;
import com.turtledev.pockercounter.netWork.commands.InsertChargeAction;


public class AddChargesDictFragment extends AbstractFragment  {
    public static final String ADD_RESULT = "ADD_RESULT";
    public static final int CONT = R.id.container;
    public static final String TAG = "AddFragChargeDictionaryTag";
    EditText etxCharge;
    DBAdapter dbAdapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_addcd_layout, container, false);
        etxCharge = (EditText) view.findViewById(R.id.et_facd);
        setActionBar();

        return view;
    }


    private void setActionBar() {
        ActionBar ab = ((MainActivity) getActivity()).getSupportActionBar();
        ab.setTitle((getResources().getString(R.string.Add_Charges)));
        ActionBarDrawerToggle toggle=((MainActivity)getActivity()).getToggle();
        toggle.setHomeAsUpIndicator(R.mipmap.ic_keyboard_backspace);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });


    }




    @Override
    public void work() {
        String result = etxCharge.getText().toString();
        if(!result.equals("")) {
            putCharges(takeSPToken(),result);

        }else {
            Toast.makeText(getActivity(),"Set all fields",Toast.LENGTH_SHORT).show();
        }
          }



    @Override
    public String getTagFragment() {
        return this.TAG;
    }

    @Override
    public int GetContFragment() {
        return  this.CONT;
    }

    private void putCharges(String userToken, final String newCharge) {
        InsertChargeAction insertChargeAction=new InsertChargeAction(takeSPToken(),new InsertCharge(newCharge),
                new NetworkCallback() {
                    @Override
                    public void onSuccess(Object response) {
                        Fragment targetFragment = getTargetFragment();
                        if (targetFragment != null) {
                            insertToDB(newCharge);
                            Intent intent = new Intent();
                            intent.putExtra(ADD_RESULT, newCharge);
                            targetFragment.onActivityResult(ChargesDictFragment.ADD_CHARGE_D_ITEM, Activity.RESULT_OK, intent);
                            etxCharge.setText("");
                        }
                        getFragmentManager().popBackStack();
                    }

                    @Override
                    public void onFailure(String error) {
                        Toast.makeText(getActivity(),"Some problems with sending data to server",Toast.LENGTH_LONG).show();
                        etxCharge.setText("");
                    }
                }, 2);
        insertChargeAction.run();
    }

    public void insertToDB(String newCharge){
        dbAdapter=new DBAdapter(getActivity());
        boolean isInserted=dbAdapter.insertChargeToDB(newCharge);
        if(isInserted){
            Toast.makeText(getActivity(), "Charges " + newCharge.toString() + " successfull add", Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(getActivity(), "Charges " + newCharge.toString() + " is noy add", Toast.LENGTH_LONG).show();
        }
    }

    public String takeSPToken(){
        SharedPreferences sPref = getContext().getSharedPreferences(myConstants.SHARED_PREF,getActivity().MODE_PRIVATE);
        String token = sPref.getString(myConstants.USER_TOKEN, null);
        return token;
    }


}
