package com.turtledev.pockercounter.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.turtledev.pockercounter.MainActivity;
import com.turtledev.pockercounter.R;
import com.turtledev.pockercounter.RecyclerAdapter.ChargesAdapter;
import com.turtledev.pockercounter.mDataBase.DBAdapter;
import com.turtledev.pockercounter.models.DetailCharge;

import java.util.ArrayList;
import java.util.List;



public class ChargesFragment extends AbstractFragment implements SearchView.OnQueryTextListener  {

    public static final int  CONT = R.id.container;
    public  static  final  String TAG ="FragChargesTag";
    public static final int ADD_CHARGE_ITEM = 1002;
    private List<DetailCharge> list;
    private Fragment mFragment;
    private ChargesAdapter chargesAdapter;
    RecyclerView rv;
    DBAdapter dbAdapter;

    public ChargesFragment() {
        setHasOptionsMenu(true);
    }



    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_c_layout,container,false);
        rv = (RecyclerView) view.findViewById(R.id.rv_charges);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        //updateUI();
        setActionBar();

        //перемутити
        dbAdapter = new DBAdapter(getActivity());
        list = dbAdapter.getDetailList();
        chargesAdapter = new ChargesAdapter(list, (ChargesAdapter.Callbacks) getActivity());
        rv.setAdapter(chargesAdapter);
        return  view;
    }

//    private void updateUI() {
//        dbAdapter = new DBAdapter(getActivity());
//        list = dbAdapter.getDetailList();
//        if (chargesAdapter == null) {
//            chargesAdapter = new ChargesAdapter(list, (ChargesAdapter.Callbacks) getActivity());
//            rv.setAdapter(chargesAdapter);
//        } else {
//
//            //chargesAdapter.notifyDataSetChanged();
//            rv.setAdapter(chargesAdapter);
//        }
//    }







    public int GetContFragment() {
        return CONT;
    }


    private void setActionBar() {
        android.support.v7.app.ActionBar ab = ((MainActivity) getActivity()).getSupportActionBar();
        ab.setTitle((getResources().getString(R.string.Charges)));
        ((MainActivity)getActivity()).getToggle().setDrawerIndicatorEnabled(true);
        }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_charges_fragment,menu);
        MenuItem searchItem = menu.findItem(R.id.search_item);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        MenuItemCompat.setOnActionExpandListener(searchItem,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem searchItem) {
                        // Do something when collapsed
                        chargesAdapter.setFilter(list);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem searchItem) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });

        super.onCreateOptionsMenu(menu, inflater);
    }
    private List<DetailCharge> filter(List<DetailCharge> list, String query) {
        query = query.toLowerCase();

        final List<DetailCharge> filterList = new ArrayList<DetailCharge>();
        for (DetailCharge charges : list) {
            final String text = charges.getParentChargeNAme().toLowerCase();
            if (text.contains(query)) {
                filterList.add(charges);
            }
        }

        return filterList;
    }
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        List<DetailCharge> mfilterList = filter(list, newText);
        chargesAdapter.setFilter(mfilterList);
        return true;
    }



    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public void work() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (mFragment == null) {
            mFragment = new AddChargeFragment();
        }
        mFragment.setTargetFragment(this, ADD_CHARGE_ITEM);
        ft.replace(this.GetContFragment(), mFragment);
        ft.addToBackStack(null);
        ft.commit();
          }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case ADD_CHARGE_ITEM:

                    String name=data.getStringExtra(AddChargeFragment.ADD_RESULT_NAME.toString());
                    double sum=data.getDoubleExtra(AddChargeFragment.ADD_RESULT_SUM,0.0);
                    Toast.makeText(getActivity(), "Name  " + name.toString() +" \n "+sum + "successfull add", Toast.LENGTH_LONG).show();
                    break;
                default:
                    super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }


}
