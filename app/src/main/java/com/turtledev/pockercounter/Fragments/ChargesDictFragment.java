package com.turtledev.pockercounter.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.turtledev.pockercounter.MainActivity;
import com.turtledev.pockercounter.R;
import com.turtledev.pockercounter.RecyclerAdapter.ChargesDictAdapter;
import com.turtledev.pockercounter.mDataBase.DBAdapter;
import com.turtledev.pockercounter.models.Charges;

import java.util.ArrayList;
import java.util.List;


public class ChargesDictFragment extends AbstractFragment implements SearchView.OnQueryTextListener {

    public static final String ARG_CHARGES_LIST = "Charges_List";

    public static final int CONT = R.id.container;
    public static final int ITEM = R.layout.fcd_item;
    public static final String TAG = "FragChargeDictionaryTag";
    public static final int ADD_CHARGE_D_ITEM = 1001;


    private Fragment mFragment;
    private ChargesDictAdapter chargesDictAdapter;
    private RecyclerView chargesDictRecyclerView;
    private DBAdapter dbAdapter;


    public ChargesDictFragment() {

    }





    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cd_layout, container, false);
        chargesDictRecyclerView = (RecyclerView) view.findViewById(R.id.rv_charge_dictionary);
        chargesDictRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        dbAdapter = new DBAdapter(getActivity());
        updateUI();
        setActionBar();
        setHasOptionsMenu(true);
        return view;
    }


    private void updateUI() {
        List<Charges> list = dbAdapter.dBListCharges();
        if (chargesDictAdapter == null) {
            chargesDictAdapter = new ChargesDictAdapter(getActivity());
            chargesDictRecyclerView.setAdapter(chargesDictAdapter);
        } else {
            //chargesDictAdapter.notifyDataSetChanged();

            chargesDictRecyclerView.setAdapter(chargesDictAdapter);

        }
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        updateUI();
//    }

    private void setActionBar() {

        android.support.v7.app.ActionBar ab = ((MainActivity) getActivity()).getSupportActionBar();
        ab.setTitle((getResources().getString(R.string.Charges_Dictionary)));
        ((MainActivity) getActivity()).getToggle().setDrawerIndicatorEnabled(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_charges_fragment, menu);
        MenuItem searchItem = menu.findItem(R.id.search_item);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        MenuItemCompat.setOnActionExpandListener(searchItem,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem searchItem) {
                        // Do something when collapsed
                        chargesDictAdapter.setFilter(dbAdapter.dBListCharges());
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem searchItem) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });

        super.onCreateOptionsMenu(menu, inflater);
    }

    private List<Charges> filter(List<Charges> list, String query) {
        query = query.toLowerCase();

        final List<Charges> filterList = new ArrayList<Charges>();
        for (Charges charges : list) {
            final String text = charges.getName().toLowerCase();
            if (text.contains(query)) {
                filterList.add(charges);
            }
        }

        return filterList;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        List<Charges> mfilterList = filter(dbAdapter.dBListCharges(), newText);
        chargesDictAdapter.setFilter(mfilterList);
        return true;
    }

    @Override
    public int GetContFragment() {
        return this.CONT;
    }

    @Override
    public int GetItemFragment() {
        return this.ITEM;
    }

    @Override
    public String getTagFragment() {
        return this.TAG;
    }

    @Override
    public void work() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (mFragment == null) {
            mFragment = new AddChargesDictFragment();
        }
        mFragment.setTargetFragment(this, ADD_CHARGE_D_ITEM);
        ft.replace(this.GetContFragment(), mFragment);
        ft.addToBackStack(null);
        ft.commit();


    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case ADD_CHARGE_D_ITEM:

                    String newCharge = data.getStringExtra(AddChargesDictFragment.ADD_RESULT);
                    chargesDictAdapter.addCharge(new Charges(newCharge));
                    //Toast.makeText(getActivity(), "Charges " + newCharge.toString() + " successfull add", Toast.LENGTH_LONG).show();
                    break;
                default:
                    super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }


}
