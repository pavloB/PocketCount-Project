package com.turtledev.pockercounter.Fragments;

import android.support.v4.app.Fragment;

import com.turtledev.pockercounter.Interfases.MyFragment;


public class AbstractFragment extends Fragment implements MyFragment {


    @Override
    public int GetContFragment() {
        return 0;
    }

    @Override
    public int GetItemFragment() {
        return 0;
    }

    @Override
    public String getTagFragment() {
        return null;
    }

    @Override
    public void work() {
    }

}
