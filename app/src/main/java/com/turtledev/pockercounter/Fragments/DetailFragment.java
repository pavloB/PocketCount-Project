package com.turtledev.pockercounter.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.turtledev.pockercounter.MainActivity;
import com.turtledev.pockercounter.R;
import com.turtledev.pockercounter.mDataBase.DBAdapter;
import com.turtledev.pockercounter.models.DetailCharge;

import java.util.List;

/**
 * Created by 1 on 10.07.2016.
 */
public class DetailFragment extends AbstractFragment {


    private static final String DETAIL_NAME = "cahrge_name";
    List<DetailCharge> list;
    private DBAdapter dbAdapter;
    RecyclerView recyclerView;
    DetaileAdapter detaileAdapter;
    String name;

    public static DetailFragment newInstance(String name) {
        Bundle args = new Bundle();
        args.putSerializable(DETAIL_NAME, name);
        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        name = (String) getArguments().getSerializable(DETAIL_NAME);
        dbAdapter=new DBAdapter(getActivity());
        list=dbAdapter.getDetailDetailList(name);

        Log.d("myLog1",name);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.detail_rv,container,false);
        recyclerView= (RecyclerView) view.findViewById(R.id.rv_detail);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        detaileAdapter=new DetaileAdapter(list);
        recyclerView.setAdapter(detaileAdapter);
        setActionBar();
        return view;
    }
    private void setActionBar() {

        android.support.v7.app.ActionBar ab = ((MainActivity) getActivity()).getSupportActionBar();
        ab.setTitle((getResources().getString(R.string.Charges)) + ": " +name );
        ((MainActivity) getActivity()).getToggle().setDrawerIndicatorEnabled(true);
    }

    private class DetailHolder extends RecyclerView.ViewHolder{

        TextView tvName,tvSum,tvDescription;
        Button btnTime;
        DetailCharge detailCharge;

        public DetailHolder(View itemView) {
            super(itemView);

            tvName=(TextView)itemView.findViewById(R.id.tvD_name);
            tvSum=(TextView)itemView.findViewById(R.id.tvD_sum);
            tvDescription=(TextView)itemView.findViewById(R.id.tvD_description);
            btnTime=(Button)itemView.findViewById(R.id.btnTime);
        }

        public void bindDetailCharge(DetailCharge detailCharge) {
            this.detailCharge=detailCharge;
            tvName.setText(detailCharge.getParentChargeNAme().toString());
            tvSum.setText(String.valueOf(detailCharge.getSum())+" $");
            tvDescription.setText(detailCharge.getDescription().toString());
            btnTime.setText(detailCharge.getTime().toString());
        }
    }

    private class DetaileAdapter extends RecyclerView.Adapter<DetailHolder>{

        List<DetailCharge> list;

        public DetaileAdapter(List<DetailCharge> list) {
            this.list = list;
        }

        @Override
        public DetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater=LayoutInflater.from(getActivity());
            View v=inflater.inflate(R.layout.detail_item,parent,false);

            return new DetailHolder(v);
        }

        @Override
        public void onBindViewHolder(DetailHolder holder, int position) {
            DetailCharge detailCharge=list.get(position);
            holder.bindDetailCharge(detailCharge);

        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }
}
