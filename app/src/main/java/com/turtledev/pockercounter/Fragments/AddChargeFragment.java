package com.turtledev.pockercounter.Fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.turtledev.pockercounter.MainActivity;
import com.turtledev.pockercounter.R;
import com.turtledev.pockercounter.mConstants.myConstants;
import com.turtledev.pockercounter.mDataBase.DBAdapter;
import com.turtledev.pockercounter.models.DetailCharge;
import com.turtledev.pockercounter.netWork.NetworkCallback;
import com.turtledev.pockercounter.netWork.commands.InsertDetailChargeAction;

import java.util.List;





public class AddChargeFragment extends AbstractFragment {
    public static final String ADD_RESULT_NAME = "ADD_RESULT_NAME";
    public static final String ADD_RESULT_SUM = "ADD_RESULT_SUM";
    public static final int CONT = R.id.container;
    public static final String TAG = "AddFragChargeTag";

    View view;
    Spinner spinner;
    ArrayAdapter<String> arrayAdapter;

    DBAdapter dbAdapter;
    EditText  etxSum,etDescription;



    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_addc_layout, container, false);

        spinner = (Spinner) view.findViewById(R.id.et_fac_name);
        etxSum = (EditText) view.findViewById(R.id.et_fac_sum);
        etDescription = (EditText) view.findViewById(R.id.et_description);

        setActionBar();
        initSpinner();


        return view;
    }


    private void setActionBar() {
        ActionBar ab = ((MainActivity) getActivity()).getSupportActionBar();
        ab.setTitle((getResources().getString(R.string.Add_Charges)));
        ActionBarDrawerToggle toggle=((MainActivity)getActivity()).getToggle();
        toggle.setHomeAsUpIndicator(R.mipmap.ic_keyboard_backspace);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });


    }

    public void initSpinner(){
        arrayAdapter=new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,createListNamesFromDB());
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
    }


    @Override
    public void work() {
        Fragment targetFragment = getTargetFragment();
        String check = etxSum.getText().toString();


        if (check == null || check.isEmpty()) {

            Toast.makeText(getActivity(), "Set all fields", Toast.LENGTH_SHORT).show();

        } else {

            if (targetFragment != null) {

                double sum = Double.parseDouble(check);
                String spinnerSelect = spinner.getSelectedItem().toString();
                String description=etDescription.getText().toString();
                DetailCharge detailCharge=new DetailCharge(spinnerSelect,description,sum);
                putDetailCharges(takeSPToken(),detailCharge);

            }



        }
    }

    private void putDetailCharges(String userToken, final DetailCharge detailCharge) {
        InsertDetailChargeAction insertDetailChargeAction=new InsertDetailChargeAction(takeSPToken(),detailCharge,
                new NetworkCallback() {
                    @Override
                    public void onSuccess(Object response) {
                        Fragment targetFragment = getTargetFragment();
                        if (targetFragment != null) {
                            insertDetailToDB(detailCharge);
                            etxSum.setText("");
                        }
                        getFragmentManager().popBackStack();
                    }

                    @Override
                    public void onFailure(String error) {
                        Toast.makeText(getActivity(),"Some problems with sending data to server",Toast.LENGTH_LONG).show();

                    }
                }, 2);
        insertDetailChargeAction.run();
    }

    public String takeSPToken(){
        SharedPreferences sPref = getContext().getSharedPreferences(myConstants.SHARED_PREF,getActivity().MODE_PRIVATE);
        String token = sPref.getString(myConstants.USER_TOKEN, null);
        return token;
    }



    public List<String> createListNamesFromDB(){
        dbAdapter=new DBAdapter(getActivity());
        List<String> listNames=dbAdapter.dBListNames();
        return  listNames;

    }

    public void insertDetailToDB(DetailCharge detailCharge){
        dbAdapter=new DBAdapter(getActivity());
        boolean isInserted=dbAdapter.insertDetailChargeToDB(detailCharge);
        if(isInserted){
            Toast.makeText(getActivity(), "Charges " + detailCharge.getParentChargeNAme().toString() + " successfull add", Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(getActivity(), "Charges " + detailCharge.getParentChargeNAme().toString() + " is noy add", Toast.LENGTH_LONG).show();
        }
    }




    @Override
    public String getTagFragment() {
        return this.TAG;
    }

    @Override
    public int GetContFragment() {
        return  this.CONT;
    }

}
