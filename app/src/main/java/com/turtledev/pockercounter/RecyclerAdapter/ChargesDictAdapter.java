package com.turtledev.pockercounter.RecyclerAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.turtledev.pockercounter.R;
import com.turtledev.pockercounter.mDataBase.DBAdapter;
import com.turtledev.pockercounter.models.Charges;

import java.util.ArrayList;
import java.util.List;


public class ChargesDictAdapter extends RecyclerView.Adapter<ChargesDictAdapter.ChargesViewHolder> {

    private DBAdapter dbAdapter;
    private List<Charges> chargesList;

    public ChargesDictAdapter(Context context) {
        dbAdapter=new DBAdapter(context);
        chargesList = dbAdapter.dBListCharges();
    }

    public void addCharge(Charges charges) {
        chargesList.add(charges);
        notifyDataSetChanged();

    }


    @Override
    public ChargesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fcd_item, parent, false);

        return new ChargesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChargesViewHolder holder, int position) {
        Charges charge= chargesList.get(position);
        holder.bindCharge(charge);


    }

    @Override
    public int getItemCount() {
        return chargesList.size();
    }


    public void setFilter(List<Charges> newChargesList) {
        chargesList = new ArrayList<Charges>();
        chargesList.addAll(newChargesList);

        notifyDataSetChanged();


    }


    public class ChargesViewHolder extends RecyclerView.ViewHolder {
        private TextView chargesItem;
        private LinearLayout ll;
        private Charges mCharge;


        public ChargesViewHolder(View itemView) {
            super(itemView);
            ll = (LinearLayout) itemView.findViewById(R.id.fcd_item_ll);
            chargesItem = (TextView) itemView.findViewById(R.id.fcd_item);
        }
        public void bindCharge(Charges charge) {
            mCharge = charge;
            chargesItem.setText(charge.getName().toString());

        }


    }
}


