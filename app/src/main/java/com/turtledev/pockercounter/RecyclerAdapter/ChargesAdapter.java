package com.turtledev.pockercounter.RecyclerAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.turtledev.pockercounter.R;
import com.turtledev.pockercounter.models.DetailCharge;

import java.util.ArrayList;
import java.util.List;

public class ChargesAdapter extends RecyclerView.Adapter<ChargesAdapter.ChargessViewHolder> {
    private List<DetailCharge> chargesList;
    private Callbacks callbacks;

    public interface Callbacks {
        void onChargeSelected(String name);
    }



    public ChargesAdapter(List<DetailCharge> mChargesList,Callbacks callbacks) {
        this.chargesList=mChargesList;
        this.callbacks=callbacks;
    }



    @Override
    public ChargessViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.fc_item, parent, false);

        return new ChargessViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ChargesAdapter.ChargessViewHolder holder, int position) {
        DetailCharge charge= chargesList.get(position);
        holder.bindCharge(charge);

    }
    @Override
    public int getItemCount() {
        return chargesList.size();
    }

    public void setFilter(List<DetailCharge> newChargesList) {
        chargesList = new ArrayList<DetailCharge>();
        chargesList.addAll(newChargesList);

        notifyDataSetChanged();
    }
    public void setDetailList(List<DetailCharge> chargesList) {
        this.chargesList = chargesList;
    }

    public  class ChargessViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        RelativeLayout rl;
        private TextView chargesItem;
        private TextView chargesItemTotal;
        private DetailCharge mCharge;




        ChargessViewHolder(View itemView) {
            super(itemView);


            rl = (RelativeLayout) itemView.findViewById(R.id.rl);
            chargesItemTotal = (TextView) itemView.findViewById(R.id.tv_fc_total);
            chargesItem = (TextView) itemView.findViewById(R.id.tv_fc_name);

            itemView.setOnClickListener(this);
        }
        public void bindCharge(DetailCharge charge) {
            mCharge = charge;
            chargesItem.setText(charge.getParentChargeNAme().toString());
            chargesItemTotal.setText(String.valueOf(charge.getSum()));
        }

        @Override
        public void onClick(View v) {
            callbacks.onChargeSelected(mCharge.getParentChargeNAme().toString());

        }
    }
}
